library ieee;
use ieee.std_logic_1164.all;

entity FF_SR is 
	port(
		S :	in	std_logic;
		R :	in	std_logic;
		Q :	out	std_logic;
		Qn:	out	std_logic
	);
end FF_SR;

architecture behavioral of FF_SR is

begin
	process (S, R)
	begin
		if S = '1' and R = '0' then
			Q <= '1';
			Qn<= '0';
		elsif S = '0' and R = '1' then
			Q <= '0';
			Qn<= '1';
		elsif S = '0' and R = '0' then
			Q <= '0';
			Qn<= '0';
		end if;
	end process;
end behavioral;